﻿using System;

namespace DesignPatternExamples
{
    /// <summary>
    ///     Metodo factory astratto. E' astratto in quanto i metodi factory dipendono dalle sue implementazioni concrete
    /// </summary>
    internal abstract class AbstractFactory
    {
        /// <summary>
        ///     Factory per creare l'oggetto <see cref="AbstractProductA" />
        /// </summary>
        public abstract AbstractProductA CreateProductA();

        /// <summary>
        ///     Factory per creare l'oggetto <see cref="AbstractProductB" />
        /// </summary>
        public abstract AbstractProductB CreateProductB();
    }


    /// <summary>
    ///     Esempio di Factory concreta, questa factory di occupa di creare le istanze concrete. Con questo pattern ho una
    ///     perfetta scorporazione in quanto il client non ha visibilità nemmeno sull'oggetto concreto che sta creando
    /// </summary>
    internal class ConcreteFactory1 : AbstractFactory
    {
        public override AbstractProductA CreateProductA()
        {
            return new ConcreteProductA1();
        }

        public override AbstractProductB CreateProductB()
        {
            return new ConcreteProductB1();
        }
    }

    /// <summary>
    ///     Esempio di Factory concreta, questa factory di occupa di creare le istanze concrete. Con questo pattern ho una
    ///     perfetta scorporazione in quanto il client non ha visibilità nemmeno sull'oggetto concreto che sta creando
    /// </summary>
    internal class ConcreteFactory2 : AbstractFactory
    {
        public override AbstractProductA CreateProductA()
        {
            return new ConcreteProductA2();
        }

        public override AbstractProductB CreateProductB()
        {
            return new ConcreteProductB2();
        }
    }

    /// <summary>
    ///     Generico prodotto A astratto
    /// </summary>
    internal abstract class AbstractProductA
    {
    }

    /// <summary>
    ///     Generico prodotto B astratto
    /// </summary>
    internal abstract class AbstractProductB
    {
        public abstract string Interact(AbstractProductA a);
    }


    /// <summary>
    ///     Prorotto concreto A1
    /// </summary>
    internal class ConcreteProductA1 : AbstractProductA

    {
    }

    /// <summary>
    ///     Prorotto concreto B1
    /// </summary>
    internal class ConcreteProductB1 : AbstractProductB
    {
        public override string Interact(AbstractProductA a)
        {
            return $"{GetType().Name} interacts with {a.GetType().Name}";
        }
    }

    /// <summary>
    ///     Prorotto concreto A2
    /// </summary>
    internal class ConcreteProductA2 : AbstractProductA

    {
    }

    /// <summary>
    ///     Prorotto concreto B2
    /// </summary>
    internal class ConcreteProductB2 : AbstractProductB

    {
        public override string Interact(AbstractProductA a)
        {
            return $"{GetType().Name} interacts with {a.GetType().Name}";
        }
    }

    /// <summary>
    ///     Oggetto che utilizza gli oggetti creati dalle factory <see cref="AbstractProductA" /> e
    ///     <see cref="AbstractProductB" />
    /// </summary>
    internal class Client
    {
        private readonly AbstractProductA _abstractProductA;
        private readonly AbstractProductB _abstractProductB;

        /// <summary>
        ///     Data la factory <paramref name="factory" /> in ingresso ho la creazione dei prodotti utilizzati dalla classe
        /// </summary>
        public Client(AbstractFactory factory)
        {
            // N.B. il client lavora con tipi astratti e inoltre non conosce l'istanza dell'oggetto creato in quanto questo dipende dalla factory.
            // In questo modo il client è aperto alle estensioni ma chiuso alle modifiche: qualsiasi cosa accada al mondo concreto questo codice non cambia
            _abstractProductB = factory.CreateProductB();
            _abstractProductA = factory.CreateProductA();
        }

        /// <summary>
        ///     Generico metodo per dimostrare che tutto funziona
        /// </summary>
        public string Run()
        {
            return _abstractProductB.Interact(_abstractProductA);
        }
    }
}