﻿namespace DesignPatternExamples
{
    /// <summary>
    ///     Oggetto generico che verrà utilizzato nel programma principale/chiamante. Questa interfaccia conterrà tutte i
    ///     metodi che dovranno essere chiamati dagli utilizzatori. Notare che il chiamante non sa di avere a che fare con un
    ///     AbstractDecorator, neanche nel nome.
    /// </summary>
    public interface ICommonInterface
    {
        string Operation();
    }

    /// <summary>
    ///     Questa classe mi serve per poter terminare la catena dei costruttori, infatti si nota che, ereditando direttamente
    ///     da <see cref="ICommonInterface" /> invece che da <see cref="AbstractDecorator" /> non necessita di un
    ///     <see cref="ICommonInterface" /> in ingresso e conseguentemente può andare bene per terminare la catena dei
    ///     costruttori con un comportamento di default che voglio avere sempre. Per esempio se prendiamo l'esempio dei caffè,
    ///     questo oggetto sarà il caffè stesso senza nulla (che ci deve sempre essere). Le classi XDecoration, YDecoration,
    ///     ZDecoration
    ///     saranno invece le varie decorazioni da aggiungere all'elemento base.
    /// </summary>
    public class CoreElement : ICommonInterface
    {
        public string Operation()
        {
            return "CoreElement";
        }
    }

    /// <summary>
    ///     Classe cardine che permette al pattern di funzionare. Questa classe non deve essere chiamata dall'esterno, ma
    ///     verranno istanziate solo sue figlie. Il costruttore della classe, avendo in ingresso una
    ///     <see cref="ICommonInterface" /> permette la costruzione a matrioska.
    /// </summary>
    public abstract class AbstractDecorator : ICommonInterface
    {
        /// <summary>
        ///     Istanza della classe da decorare
        /// </summary>
        private readonly ICommonInterface _core;

        /// <summary>
        ///     Dato che tutte le classi figlie erediteranno da questo costruttore, avrò la possibilità di avere la costruzione new
        ///     XDecoration(new YDecoration(new ZDecoration....)))
        /// </summary>
        public AbstractDecorator(ICommonInterface core)
        {
            _core = core;
        }

        /// <summary>
        ///     Metodo overridato da <see cref="ICommonInterface" /> il quale di occupa di chiamare se stesso dell'oggetto che sto
        ///     decorando più la mia implementazione concreta, definita in <see cref="ConcreteOperation" />
        /// </summary>
        public string Operation()
        {
            return $"{_core.Operation()} with {ConcreteOperation()}";
        }

        protected abstract string ConcreteOperation();
    }

    /// <summary>
    ///     Decorazione dell'oggetto <see cref="CoreElement" /> con l'aggiunta di "XDecoration".
    /// </summary>
    public class XDecoration : AbstractDecorator
    {
        public XDecoration(ICommonInterface core) : base(core)
        {
        }

        protected override string ConcreteOperation()
        {
            return "XDecoration";
        }
    }

    /// <summary>
    ///     Decorazione dell'oggetto <see cref="CoreElement" /> con l'aggiunta di "YDecoration".
    /// </summary>
    public class YDecoration : AbstractDecorator
    {
        public YDecoration(ICommonInterface core) : base(core)
        {
        }

        protected override string ConcreteOperation()
        {
            return "YDecoration";
        }
    }

    /// <summary>
    ///     Decorazione dell'oggetto <see cref="CoreElement" /> con l'aggiunta di "XDecoration".
    /// </summary>
    public class ZDecoration : AbstractDecorator
    {
        public ZDecoration(ICommonInterface core) : base(core)
        {
        }

        protected override string ConcreteOperation()
        {
            return "ZDecoration";
        }
    }
}