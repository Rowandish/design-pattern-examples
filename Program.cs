﻿using System;

namespace DesignPatternExamples
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // Decorator
            Console.WriteLine("--- Decorator ---");
            ICommonInterface[] vars =
            {
                new XDecoration(new CoreElement()), new YDecoration(new XDecoration(new CoreElement())),
                new ZDecoration(new YDecoration(new XDecoration(new CoreElement())))
            };
            foreach (var item in vars)
            {
                Console.WriteLine(item.Operation());
            }
            // Abstract factory
            Console.WriteLine("--- Abstract Factory ---");

            var client1 = new Client(new ConcreteFactory1());
            Console.WriteLine(client1.Run());
            var client2 = new Client(new ConcreteFactory2());
            Console.WriteLine(client2.Run());

            Console.WriteLine("--- State ---");
            // Inizializzo il Context con lo stato concreto ConcreteStateA
            var c = new Context(new ConcreteStateA());
            // Lancio varie richieste, ognuna di queste modifica lo stato interno del Context
            c.Request();
            c.Request();
            c.Request();
            c.Request();


            Console.ReadKey();
        }
    }
}