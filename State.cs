﻿using System;

namespace DesignPatternExamples
{
    /// <summary>
    ///     Classe che identifica lo stato della classe <see cref="Context" />. Il metodo fondamentale è il metodo
    ///     <see cref="Handle" /> che *deve* avere in ingresso il <see cref="Context" /> e permette di modificarne il suo
    ///     <see cref="Context.State" /> in base a determinate condizioni.
    ///     Il trucco sta proprio nell'avere all'interno dello stato <see cref="State" /> una istanza del contesto
    ///     <see cref="Context" /> e che questo abbia una property public per poterne modificare lo stato.
    /// </summary>
    public abstract class State
    {
        public abstract void Handle(Context context);
    }

    /// <summary>
    ///     Generico stato A del <see cref="Context" />
    /// </summary>
    public class ConcreteStateA : State
    {
        /// <summary>
        ///     Una volta fatte tutte le operazioni dello stato A, questo viene modificato a B se vi sono determinate condizioni.
        ///     In questo caso viene solo modificato in <see cref="ConcreteStateB" />
        /// </summary>
        public override void Handle(Context context)
        {
            context.State = new ConcreteStateB();
        }
    }

    /// <summary>
    ///     Generico stato B del <see cref="Context" />
    /// </summary>
    public class ConcreteStateB : State
    {
        /// <summary>
        ///     Una volta fatte tutte le operazioni dello stato B, questo viene modificato a A se vi sono determinate condizioni.
        ///     In questo caso viene solo modificato in <see cref="ConcreteStateA" />
        /// </summary>
        public override void Handle(Context context)
        {
            context.State = new ConcreteStateA();
        }
    }

    /// <summary>
    ///     Oggetto che ha uno stato <see cref="State" /> che viene modificato nel tempo
    /// </summary>
    public class Context
    {
        /// <summary>
        ///     Stato della classe
        /// </summary>
        private State _state;

        /// <summary>
        ///     Costruttore: serve per inizializzare lo stato <see cref="State" />
        /// </summary>
        public Context(State state)
        {
            State = state;
        }

        /// <summary>
        ///     Metodo fondamentale, in quanto permette agli stati <see cref="State" /> di modificare lo stato del Context
        /// </summary>
        public State State
        {
            set
            {
                _state = value;
                Console.WriteLine($"State: {_state.GetType().Name}");
            }
        }

        /// <summary>
        ///     Generico metodo verso il mondo esterno. Il comportamento di questo metodo cambia in base allo stato
        ///     <see cref="_state" />
        /// </summary>
        public void Request()
        {
            _state.Handle(this);
        }
    }
}